@tool
extends EditorPlugin

const MainPanel = preload("res://addons/nas_codecks/nas_codecks_main_panel.tscn")
var main_panel_instance

func _enter_tree():
	main_panel_instance = MainPanel.instantiate()
	# Add the main panel to the editor's main viewport.
	get_editor_interface().get_editor_main_screen().add_child(main_panel_instance)
	# Hide the main panel. Very much required.
	_make_visible(false)
	initialize_settings()

func initialize_settings():
	connect("project_settings_changed",Callable(self, "_on_project_settings_changed"))
	if !ProjectSettings.has_setting("plugin/nas_codecks/subdomain"):
		ProjectSettings.set_setting("plugin/nas_codecks/subdomain","")
	else: main_panel_instance.subdomain = ProjectSettings.get_setting("plugin/nas_codecks/subdomain")
	if !ProjectSettings.has_setting("plugin/nas_codecks/api_token"):
		ProjectSettings.set_setting("plugin/nas_codecks/api_token","")
	else: main_panel_instance.subdomain = ProjectSettings.get_setting("plugin/nas_codecks/api_token") 
	if !ProjectSettings.has_setting("plugin/nas_codecks/project_ids"):
		ProjectSettings.set_setting("plugin/nas_codecks/project_ids","")

func _on_project_settings_changed():
	if ProjectSettings.has_setting("plugin/nas_codecks/subdomain"): main_panel_instance.subdomain = ProjectSettings.get_setting("plugin/nas_codecks/subdomain")
	if ProjectSettings.has_setting("plugin/nas_codecks/api_token"): main_panel_instance.api_token = ProjectSettings.get_setting("plugin/nas_codecks/api_token")

func _exit_tree():
	disconnect("project_settings_changed",Callable(self, "_on_project_settings_changed"))
	if main_panel_instance:
		main_panel_instance.queue_free()

func _has_main_screen():
	return true

func _make_visible(visible):
	if main_panel_instance:
		main_panel_instance.visible = visible
		if visible: main_panel_instance.load_codecks_data()

func _get_plugin_name():
	return "Codecks"

func _get_plugin_icon():
	return preload("res://addons/nas_codecks/assets/codecks_icon_1.svg")
