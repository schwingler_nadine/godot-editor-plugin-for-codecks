@tool
extends Control

# Start Time Tracking: /dispatch/timeTracking/start => cardId, sessionId, userId POST
# Stop Time Tracking: /dispatch/timeTracking/stop => id, sessionId POST
# Mark as Done: /dispatch/cards/update => id, sessionId, status: 'done' POST
# status possible: 'done', 'not_started', 'started

@onready var deck_prefab = preload("res://addons/nas_codecks/elements/deck.tscn")
@onready var card_prefab = preload("res://addons/nas_codecks/elements/card.tscn")

@onready var api_connection = $GetDataConnect
@onready var timer_connection = $TimerConnect
@onready var update_connection = $UpdateConnect

@onready var settings_panel = $Settings
@onready var project_list_container = $Settings/Content/Projects

@onready var create_card_panel = $CreateCard

@onready var hand_container = $MarginContainer/Content/Hand
@onready var card_content_container = $MarginContainer/Content/BottomContent/CardContent

var card_size = Vector2(115,147)

var subdomain:String
var api_token:String

var api_url = "https://api.codecks.io/"

var account
var logged_in_user
var projects

var current_tracker_id

var decks
var cards
var hand
var totalTrackedSums
var ordered_hand:Array

func _on_card_clicked(card):
	card_content_container.show_content(card.get_parent_string(),card.card_data.content)

func _on_card_started(card, started):
	for c in hand_container.get_children():
		if c != card: c.started = false
		c.set_button_texture()
	if started: start_timer(card)
	else: stop_timer(card)

func _on_card_done(card):
	var parameter = {
		"id": card.card_data.cardId,
		"status": "done"
	}
	call_update_api(parameter, "dispatch/cards/update")

func start_timer(card):
	var parameter = {
		"cardId": card.card_data.cardId,
		"userId": logged_in_user.id
	}
	call_timer_api(parameter, "dispatch/timeTracking/start")

func stop_timer(card):
	if current_tracker_id != null:
		var parameter = {
			"id": current_tracker_id
		}
		call_timer_api(parameter, "dispatch/timeTracking/stop")
		current_tracker_id = null

func display_hand():
	clear_hand()
	prepare_hand()
	for c in ordered_hand:
		var new_card = card_prefab.instantiate()
		c["deck_name"] = decks[c.deck].title
		c["project_name"] = projects[decks[c.deck].project].name
		new_card.set_data(c, account.timeTrackingMode)
		new_card.connect("card_started", Callable(self, "_on_card_started"))
		new_card.connect("card_done", Callable(self, "_on_card_done"))
		new_card.connect("card_clicked", Callable(self, "_on_card_clicked"))
		hand_container.add_child(new_card)

func clear_hand():
	for c in hand_container.get_children():
		c.queue_free()

func prepare_hand():
	var width = hand_container.size.x
	var cols = width / (card_size.x + 25)
	hand_container.columns = floor(cols)
	order_hand()

func order_hand():
	ordered_hand = []
	var preordered_hand = {}
	var keys = []
	for c in hand:
		var ca = hand[c].card
		if ca in cards:
			var card = cards[ca]
			if hand[c].sortIndex != null:
				match card.status:
					"not_started","started":
						if card.deck in decks && check_show_project(decks[card.deck].project):
							preordered_hand[hand[c].sortIndex] = card
							keys.append(hand[c].sortIndex)
					"done": pass
					_: print(card.status)
	keys.sort()
	for k in keys:
		ordered_hand.append(preordered_hand[k])

func check_show_project(project_id)->bool:
	return ProjectSettings.get_setting("plugin/nas_codecks/project_ids").contains(project_id)

func list_projects():
	clear_projects()
	if projects.size() == 0: return
	for p in projects.values():
		var pro = CheckButton.new()
		pro.text = p.name
		pro.connect("toggled", Callable(self, "_on_project_setting_toggled").bind(p.id))
		if  ProjectSettings.get_setting("plugin/nas_codecks/project_ids").contains(p.id): pro.button_pressed = true
		project_list_container.add_child(pro)

func _on_project_setting_toggled(button_pressed:bool, project_id:String):
	var current_settings = ProjectSettings.get_setting("plugin/nas_codecks/project_ids")
	var current = []
	if current_settings != "":
		current = current_settings.split(",")
	if !button_pressed && project_id in current:
		current.remove_at(current.rfind(project_id))
	if button_pressed && !project_id in current:
		current.append(project_id)
	var new_settings = ",".join(current)
	if new_settings != current_settings:
		ProjectSettings.set_setting("plugin/nas_codecks/project_ids", new_settings)
		load_codecks_data()

func clear_projects():
	for p in project_list_container.get_children():
		p.queue_free()

func load_codecks_data():
	if !api_connection.is_connected("request_completed", Callable(self, "_on_api_request_completed")):
		api_connection.connect("request_completed", Callable(self, "_on_api_request_completed"))
	if !timer_connection.is_connected("request_completed", Callable(self, "_on_timer_request_completed")):
		timer_connection.connect("request_completed", Callable(self, "_on_timer_request_completed"))
	if !update_connection.is_connected("request_completed", Callable(self, "_on_update_request_completed")):
		update_connection.connect("request_completed", Callable(self, "_on_update_request_completed"))
	get_base_data()

func get_base_data():
	call_api(get_base_request())

func get_header() -> Array:
	var header = ["X-Account: "+subdomain,"Content-Type: application/json","X-Auth-Token: "+api_token]
	return header

func call_update_api(request_data:Dictionary, url_addition:String = ""):
	update_connection.request(api_url+url_addition, get_header(), HTTPClient.METHOD_POST, JSON.stringify(request_data))

func call_timer_api(request_data:Dictionary, url_addition:String = ""):
	timer_connection.request(api_url+url_addition, get_header(), HTTPClient.METHOD_POST, JSON.stringify(request_data))

func call_api(request_data:Dictionary, url_addition:String = ""):
	api_connection.request(api_url+url_addition, get_header(), HTTPClient.METHOD_POST, JSON.stringify(request_data))

func get_base_request()->Dictionary:
	var request = {
		"query": {
			"_root": [
				{ 
					"loggedInUser": [
						"name",
						"id"
					],
					"account":[
						"name",
						"timeTrackingMode",
						{
							"queueEntries({\"cardDoneAt\":null})":[
								"sortIndex",
								{
									"card": [
										"title",
										"status",
										"deck",
										"content",
										{
											"totalTimeTrackingSums":["sumMs"]
										}
									]
								}
							],
							"projects":[
								"id",
								"name"
							],
							"decks":[
								"id",
								"title",
								"project"
							]
						}
					]
				}
			]
		}
	}	
	return request

func _on_update_request_completed(result, response_code, headers, body):
	var json = JSON.parse_string(body.get_string_from_utf8())
	get_base_data()

func _on_timer_request_completed(result, response_code, headers, body):
	var json = JSON.parse_string(body.get_string_from_utf8())
	if "actionId" in json:
		var tracker_request = {"query": {"_root": [{ "account":[{"timeTrackingSegments({\"userId\":\""+logged_in_user.id+"\",\"finishedAt\":null})":[]}]}]}}
		call_timer_api(tracker_request)
	if "timeTrackingSegment" in json:
		for tt in json.timeTrackingSegment:
			current_tracker_id = tt

func _on_api_request_completed(result, response_code, headers, body):
	var json = JSON.parse_string(body.get_string_from_utf8())
	if "account" in json:
		for a in json.account:
			account = json.account[a]
	if "user" in json:
		for u in json.user.values():
			logged_in_user = u
	if "project" in json:
		projects = json.project
	if "card" in json:
		cards = json.card
	if "deck" in json:
		filter_decks(json.deck)
	if "timeTrackingSum" in json:
		add_tracked_sums(json.timeTrackingSum)
	if "queueEntry" in json:
		hand = json.queueEntry
		display_hand()

func filter_decks(deck_json):
	decks = {}
	#print(deck_json)
	for d in deck_json:
		var d_values = deck_json[d]
		if check_show_project(d_values.project):
			d_values["project_name"] = projects[d_values.project].name
			decks[d] = d_values
	#print(decks)

func add_tracked_sums(tracked_sums)->Dictionary:
	var new_one = {}
	for ts in tracked_sums.values():
		cards[ts.cardId]["trackedSum"] = ts.sumMs
	return new_one

func _on_settings_button_pressed():
	list_projects()
	settings_panel.position = $MarginContainer/Content/Menu/SettingsButton.position - Vector2(30, - settings_panel.size.y / 2)
	settings_panel.visible = !settings_panel.visible

func _on_gui_input(event):
	if event is InputEventMouseButton:
		if event.button_index == MOUSE_BUTTON_LEFT:
			if event.pressed: card_content_container.clear_content()

func _on_create_card_button_pressed():
	clear_create_card_panel()
	create_card_panel.position = $MarginContainer/Content/Menu/SettingsButton.position - Vector2(30, - create_card_panel.size.y / 2)
	create_card_panel.visible = true

func _on_create_button_pressed():
	var deckId = $CreateCard/MarginContainer/Content/Deck/DeckSelect.get_item_metadata($CreateCard/MarginContainer/Content/Deck/DeckSelect.selected)
	if deckId != "":
		var parameter = {
			"content": $CreateCard/MarginContainer/Content/TextEdit.text,
			"deckId": deckId
		}
		call_update_api(parameter, "dispatch/cards/create")
		create_card_panel.visible = false

func _on_cancel_button_pressed():
	create_card_panel.visible = false

func clear_create_card_panel():
	clear_project_select()
	clear_deck_select()
	$CreateCard/MarginContainer/Content/TextEdit.text = ""
	fill_project_select()

func clear_project_select():
	$CreateCard/MarginContainer/Content/Project/ProjectSelect.clear()
	$CreateCard/MarginContainer/Content/Project/ProjectSelect.add_item("-")

func fill_project_select():
	var counter = 1
	for p in projects:
		if check_show_project(projects[p].id):
			$CreateCard/MarginContainer/Content/Project/ProjectSelect.add_item(projects[p].name,counter)
			$CreateCard/MarginContainer/Content/Project/ProjectSelect.set_item_metadata(counter,projects[p].id)
			counter += 1

func clear_deck_select():
	$CreateCard/MarginContainer/Content/Deck/DeckSelect.clear()
	$CreateCard/MarginContainer/Content/Deck/DeckSelect.add_item("-")

func fill_deck_select(project_id):
	clear_deck_select()
	var counter = 1
	for d in decks:
		if decks[d].project == project_id:
			$CreateCard/MarginContainer/Content/Deck/DeckSelect.add_item(decks[d].title,counter)
			$CreateCard/MarginContainer/Content/Deck/DeckSelect.set_item_metadata(counter,decks[d].id)
			counter += 1

func _on_project_select_item_selected(index):
	if index > 0: 
		fill_deck_select($CreateCard/MarginContainer/Content/Project/ProjectSelect.get_item_metadata(index))
	else: clear_deck_select()
