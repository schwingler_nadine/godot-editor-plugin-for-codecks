@tool
extends Panel

var play_texture = load("res://addons/nas_codecks/assets/play_icon_2.svg")
var stop_texture = load("res://addons/nas_codecks/assets/stop_icon_2.svg")

var card_data
var tracking_mode

var time_factor = 3600000

var started:bool

signal card_started
signal card_done
signal card_clicked

func get_parent_string()->String:
	return card_data.project_name+"\n"+card_data.deck_name

func set_data(_card_data, _time_tracking_mode):
	card_data = _card_data
	if card_data.status == "started": started = true
	tracking_mode = _time_tracking_mode
	$Title.text = card_data.title
	$Parents.text = get_parent_string()
	show_timetracking()
	set_button_texture()

func show_timetracking():
	if tracking_mode == "strict": 
		$TimeTrack.text = get_total_tracked_time()
		$PlayButton.visible = true
		$PlayButton.icon = play_texture
	else:
		$TimeTrack.text = ""
		$PlayButton.visible = false
	
func get_total_tracked_time() -> String:
	var time_string = ""
	if "trackedSum" in card_data:
		var millis = card_data.trackedSum
		var hour_prep = millis/time_factor
		var hours = floor(hour_prep)
		var minutes = floor((hour_prep - hours) * 60)
		time_string = "%02d:%02d" % [hours, minutes]
	return time_string

func set_button_texture():
	if started: 
		$PlayButton.icon = stop_texture
		self.self_modulate = Color(0.5,0.75,0.5,1)
	else: 
		$PlayButton.icon = play_texture
		self.self_modulate = Color.WHITE

func _on_play_button_pressed():
	started = !started
	emit_signal("card_started",self, started)

func _on_done_button_pressed():
	emit_signal("card_done", self)

func _on_gui_input(event):
	if event is InputEventMouseButton:
		if event.button_index == MOUSE_BUTTON_LEFT:
			if event.pressed: emit_signal("card_clicked", self)
