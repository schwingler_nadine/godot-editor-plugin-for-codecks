@tool
extends Panel

@onready var content_container = $VBoxContainer
@onready var content = $VBoxContainer/Content
@onready var parents = $VBoxContainer/Parents

var show_width:int = 300

func _ready():
	clear_content()

func show_content(_parents:String,_text:String):
	parents.text = _parents
	content.text = _text
	visible = true

func clear_content():
	content.text = ""
	parents.text = ""
	visible = false
