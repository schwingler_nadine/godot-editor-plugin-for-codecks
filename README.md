# Godot Editor Plugin for Codecks

## What is it about?

If you use Godot and Codecks, you can now access your Codecks Card directly in the Godot Editor.
Currently you can do:
- show your hand cards
- show content of card
- start/stop time tracking of these cards
- mark cards as done
- add cards to decks

Opendeck for this project: https://open.codecks.io/godot_editor_plugin

Please be aware that this is a side project, but i will work on it.

What you need for it to work:
- Your subdomain
- Your Api-Key. (How to get it: check the api manual of codecks: https://manual.codecks.io/api/)

To start with the plugin:
- download it and add the nas_codecks to your addons folder
- activate the plugin and go to your project settings. (You may have to enable the enhanced settings) Scroll to the bottom, there should be a Plugin -> Nas Codecks section
- enter your subdomain and api key. The third field will be filled in the plugin itself, you can close the settings
- At the top bar there should be a "Codecks" Tab next to "D, Script, AssetLib, etc.
- Click on it and open the settings (right top icon), there you can activate the Project whose cards you want to see
- Cards in your hand that are in projects not selected as active here are not shown

This is just a first rough draft, i currently use myself. Will improve it over time.
Working on the Demo of my game current... so be patient

## Update-Notes: Update to 0.2.0
- You can now click on the card and the plugin will show the content of the card
- The card and the content panel now show the project and the deck name
- You can now add cards to projects and decks. Only Deck and Content are set, all other stuff needs to be done in codecks

